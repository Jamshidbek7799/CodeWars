package com.example.problems.leetcode;

import java.util.ArrayList;
import java.util.List;
import javax.crypto.spec.PSource;

public class Problem_442_Medium {
  public static void main(String[] args) {
    Problem_442_Medium solution = new Problem_442_Medium();
    List<Integer> response = solution.findDuplicates(new int[]{4, 3, 2, 7, 8, 2, 3, 1});
    System.out.println(response);
  }


  public List<Integer> findDuplicates(int[] nums) {
    List<Integer> result = new ArrayList<>();

    for (int num : nums) {
      int index = Math.abs(num) - 1;
      if (nums[index] < 0) {
        result.add(Math.abs(num));
      } else {
        nums[index] = -nums[index];
      }
    }
    return result;
  }
}




