package com.example.problems.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Problem_350_easy {

    public static void main(String[] args) {
      var response = intersect(new int[]{1,2,2,1}, new int[]{2,2});
        System.out.println();
    }

  public static int[] intersect(int[] nums1, int[] nums2) {
    Arrays.sort(nums1);
    Arrays.sort(nums2);

    List<Integer> intersectionList = new ArrayList<>();

    int pointer1 = 0, pointer2 = 0;

    while (pointer1 < nums1.length && pointer2 < nums2.length) {
      if (nums1[pointer1] == nums2[pointer2]) {
        intersectionList.add(nums1[pointer1]);
        pointer1++;
        pointer2++;
      } else if (nums1[pointer1] < nums2[pointer2]) {
        pointer1++;
      } else {
        pointer2++;
      }
    }
    int[] intersection = new int[intersectionList.size()];
    for (int i = 0; i < intersectionList.size(); i++) {
      intersection[i] = intersectionList.get(i);
    }
    return intersection;
  }
}
